\documentclass[t,aspectratio=169]{beamer}
\usepackage{redhat}
\usepackage{color}
\usepackage{layout}

% (setq TeX-engine 'xetex)
\usepackage{listings}
\usepackage{textpos}
\usepackage{fontspec}
\setmainfont{Overpass}
\setsansfont{Overpass}
\usepackage{ulem}
\usepackage{tikz}
\usetikzlibrary{tikzmark,fit,shapes.geometric,decorations.pathreplacing,angles,quotes}
\usetikzlibrary{arrows,decorations.markings}

\title{Dash and Plotly for interactive plotting}
\subtitle{\vspace{1em}Tutorial with a case-study of the Bifurcation Diagram}
\author{}
\institute{Kevin Pouget, Red Hat SPICE Senior Software Engineer}

\tikzset{
  every overlay node/.style={
    draw=black,fill=white,rounded corners,anchor=north west,
  },
}
\def\tikzoverlay{%
   \tikz[baseline,overlay]\node[every overlay node]
}%

\usepackage{listings}

\begin{document}

\maketitle

\begin{frame}{Dash+Plotly}{Presentation}

  \begin{block}{Dash\hfill{}\url{https://plot.ly/dash/}}
    \begin{itemize}
    \item ``Dash is the fastest way to build interactive analytic
      apps'' {\scriptsize (says their website)}
    \item Open source under MIT licensing
    \item Dash is available for both Python and R {\scriptsize
        (similar to RStudio)}
      \begin{itemize}
      \item good\&illustrated documentation: \url{https://dash.plot.ly/}
      \end{itemize}
    \end{itemize}
  \end{block}

  \begin{exampleblock}{Plotly}
    \begin{itemize}
    \item HTML/JS/SVG plotting \alert{from} Python
    \item many ways to customize graphs
    \item works with or without Dash
      \begin{itemize}
      \item good\&illustrated documentation: \url{https://plot.ly/python/}
      \end{itemize}
    \end{itemize}
  \end{exampleblock}

  \begin{alertblock}{}
    \texttt{pip install --user dash==1.8.0 \# installs plotly
          as well}

  \end{alertblock}

\end{frame}

\defverbatim[colored]\lstDashHtml{
\lstset{morekeywords={html,H1,Div,P,import,as}}
\begin{lstlisting}[basicstyle=\ttfamily,keywordstyle=\bf{}]
import dash_html_components as html
app.layout = html.Div(children=[
    html.H1(
        children='Hello Dash',
        style={'textAlign': 'center',
               'color': colors['text']}),

    html.Div(
        id='my-div',
        children='Dash: A web app framework for Python.',
        style={'textAlign': 'center',
               'color': colors['text']
    }),
])
\end{lstlisting}
}

\begin{frame}{Dash layout}{HTML ... in Python}
  \lstDashHtml
\end{frame}

\defverbatim[colored]\lstDashDcc{
\lstset{morekeywords={dcc,Graph,Dropdown,Checklist,RadioItems,Slider,Tabs,Tab,import,as}}
\begin{lstlisting}[basicstyle=\ttfamily,keywordstyle=\bf{}]
import dash_core_components as dcc
dcc.Dropdown(value='MTL', options=[
                {'label': 'New York City', 'value': 'NYC'},
                {'label': 'Montréal', 'value': 'MTL'},
                {'label': 'San Francisco', 'value': 'SF'}])

dcc.Checklist(...), dcc.RadioItems(...)
dcc.Slider(min=0, max=9, value=5)

dcc.Tabs(value='tab-1-example', children=[
         dcc.Tab(label='tab one', value='tab-1-example'),
         dcc.Tab(label='tab two', value='tab-2-example')])

dcc.Graph(id='example-graph-2', figure={'data': [...], 'layout': {...}})
\end{lstlisting}
}

\begin{frame}{Dash layout}{HTML ... in Python ... plus complex components}
  \lstDashDcc
\end{frame}



\defverbatim[colored]\lstDashCallbacks{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(
    Output('my-div', 'children'),
    [Input('my-slide-id', 'value')])
def update_output(slide_value):
    return f"You've entered '{slide_value}'"
\end{lstlisting}
}

\defverbatim[colored]\lstDashGraphCallbacks{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(
    Output('my-graph', 'figure'),
    [Input('my-slide-id', 'value')])
def update_graph(slide_value):
    return go.Figure(data=go.Scatter(x=[...]), y=[...]))
\end{lstlisting}
}

\begin{frame}{Dash callbacks}{HTML ... in Python ... plus complex
    components \alert{and callbacks!}}
  \lstDashCallbacks
\noindent\rule{2cm}{0.4pt}
\alt<1>{
  \begin{exampleblock}{}
    \begin{itemize}
    \item when the \texttt{value} of \texttt{my-slide-id} changes,
    \item then \texttt{update\_outout(value)} gets called.
    \item and its return value replaces \texttt{my-div}'s \texttt{children}.
    \end{itemize}
  \end{exampleblock}
}{
  \begin{alertblock}{}
    \lstDashGraphCallbacks
  \end{alertblock}
}\pause
\end{frame}

\begin{frame}{DASH callbacks}{}\vspace{-1.5em}
  \begin{block}{you can get Dash callbacks from ...}
    \begin{itemize}
    \item button clicks, text (\texttt{Div}/\texttt{P}) clicks
    \item dropdown list value entered/changed
    \item graph hover/click on value
    \item period timers, URL address change, ...
    \end{itemize}
  \end{block}
\pause
  \begin{exampleblock}{from Dash callbacks, you can ...}
    \begin{itemize}
    \item update \texttt{input} values
    \item generate new HTML elements
    \item update the CSS style of layout HTML elements
    \item generate any kind of plotly graph
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}{DASH callbacks}{}
  \begin{alertblock}{from Dash callbacks, you CANNOT ...}
    \begin{itemize}
    \item use global variables to store anything
      \begin{itemize}
      \item multi-process WSGI backends might bite you :)
      \end{itemize}
    \item set callbacks to generated HTML elements
      \begin{itemize}
      \item callbacks+inputs/outputs must be defined at app creation time
      \item workaround: use CSS style to hide/show elements instead
        (\texttt{display: none})
      \end{itemize}
    \item have more than one callback updating a given property
        \begin{itemize}
        \item {\scriptsize You have already assigned a callback to the output
                with ID ``...'' and property ``...''. An output can only have
                a single callback function. Try combining your inputs and
                callback functions together into one function.}
        \end{itemize}
    \item have dependency cycles (X generates Y, Y generates X)
    \end{itemize}
  \end{alertblock}
\end{frame}

\section{Case Study: Bifurcation Diagram}

\newcommand{\boundellipse}[3]% center, xdim, ydim
{(#1) ellipse (#2 and #3)
}

\newcommand{\insertlink}{%
  \begin{tikzpicture}[remember picture,overlay]
    \node[xshift=7.5cm,yshift=0.4cm] at (current page.south west){%
      git clone \url{https://github.com/kpouget/bifurq}};
  \end{tikzpicture}
}

\begin{frame}{Case Study: Bifurcation Diagram}\insertlink{}
  \only<1>{
    \tikzoverlay at (-0cm,0.76cm) {\includegraphics[width=13.7cm]{img/bifurk/screen2.png}};
  }%
  \only<4>{
    \tikzoverlay at (-0cm,0.76cm) {\includegraphics[width=13.7cm]{img/bifurk/screen.png}};
  }%
  \only<1>{
    \begin{tikzpicture}[remember picture,overlay] % at (2,-6.1)
      \node[inner sep=1.5pt,label={60:{$x_{n+1}=rx_{n}(1-x_{n}) | x_{0}=0.5 | r=2.9$}}] at (2,-6.1) {};
    \end{tikzpicture}
  }
  \only<3>{
    \tikzoverlay at (-0cm,0.76cm) {\includegraphics[width=13.7cm]{img/bifurk/screen-cb.png}};
  }
  \pause
  \only<2>{
  \tikzoverlay at (-0cm,0.76cm) {\includegraphics[height=4cm]{img/bifurk/Logistic_Map_Bifurcation_Diagram,_Matplotlib.png}};
  \tikzoverlay at (7cm,0.76cm) {\includegraphics[height=4cm]{img/bifurk/veritasium.png}};
  \vspace{9.15em}
  {\footnotesize
    \begin{itemize}
    \item \alert{Case-study repository} (check the branches)
      \begin{itemize}
      \item \url{https://github.com/kpouget/bifurq}
      \end{itemize}
    \item Veritasium video \textit{``This equation will change how you see the world''}
      \begin{itemize}
      \item \url{https://www.youtube.com/watch?v=ovJcsL7vyrk}
      \end{itemize}
    \item Matplotlib Bifurcation diagram my Morn, Creative CC BY SA
    \end{itemize}\vspace{-0.5em}
\url{https://en.wikipedia.org/wiki/File:Logistic_Map_Bifurcation_Diagram,_Matplotlib.svg}
  }
}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \only<3>{
      \draw [red!80,ultra thick] \boundellipse{0.7,0.4}{1}{0.3};
      \node[inner sep=1.5pt,label={60:Callback 1}] at (1.5,0.1) {};
      \draw[fill=cyan] (11.9,-2.6) circle (0cm) node[text=red] {1};

      \draw [red!80,ultra thick] \boundellipse{5,-4.4}{2.3}{1.5};
      \node[inner sep=1.5pt,label={60:Callback 2}] at (4,-3) {};
      \draw[fill=cyan] (11.4,-2.6) circle (0cm) node[text=red] {2};

      \draw [red!80,ultra thick] \boundellipse{0.5,-1.25}{1}{0.3};
      \node[inner sep=1.5pt,label={60:Callback 3}] at (1.6,-1.5) {};
      \draw[fill=cyan] (8.85,-1.35) circle (0cm) node[text=red] {3};
    }
    \pause
    \draw [red!80,ultra thick] \boundellipse{10.5,-4.4}{2.3}{1.5};
    \node[inner sep=1.5pt,label={60:Exercise 1}] at (10,-3) {};

    \draw [red!80,ultra thick] \boundellipse{0.6,-4.8}{1}{0.5};
    \node[inner sep=1.5pt,label={60:Exercise 2}] at (1.6,-5.1) {};

    \draw [red!80,ultra thick] \boundellipse{0.2,-5.65}{0.6}{0.3};
    \node[inner sep=1.5pt,label={60:Homework with solution},red] at (0.9,-6) {};
  \end{tikzpicture}
\end{frame}

\defverbatim[colored]\lstBifurkInitValue{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(
    Output('span-initial-value', 'children'),
    [Input('input-initial-value', 'value')])
def update_initial_value(value):
    return str(value) # or f"{value*100:.0f}%"
\end{lstlisting}
}

\defverbatim[colored]\lstBifurkFocus{\vspace{-1.5em}
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(
    Output('graph-focus', 'figure'),
    [Input('input-initial-value', 'value'),
     Input('input-focus-coef', 'value'),
     Input('input-show-full', 'value')]) # on/off actually
def draw_focus(init_value, coef, full):
    x = range(N_COMPUTE) if full else \
        range(N_COMPUTE - KEEP, N_COMPUTE)
    y = compute_evolution(init_value, coef, full=full)

    fig = go.Figure(data=go.Scatter(x=list(x), y=y))
    fig.update_layout(title={'text': "Population Evolution"})

    return fig
\end{lstlisting}
}

\defverbatim[colored]\lstBifurkZoom{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(
    [Output('input-start-coef', 'value'),
     Output('input-end-coef', 'value'),
     Output('input-focus-coef', 'value')],
    [Input('input-zoom', 'value'),
     Input('graph-overview', 'clickData')])
def update_coef(zoom, clickData):
    trigger = dash.callback_context.triggered[0]["prop_id"]
    if trigger.startswith('graph-overview'):
        # triggered by click on graph-overview point
        return [no_update]*2, clickData['points'][0]['x']

    # triggered by zoom-input value changed
    try: return ZOOMS[zoom]
    except KeyError: return START_COEF, END_COEF, FOCUS_COEF
\end{lstlisting}
}

\begin{frame}
  \only<1,2>{\frametitle{DASH callback 1}\framesubtitle{Update initial-value label}}
  \only<3,4>{\frametitle{DASH callback 2}\framesubtitle{Draw focus graph}}
  \only<5,6>{\frametitle{DASH callback 3: Zoom on coef}\vspace{-2.5em}}
  \only<1,3,5>{\tikzoverlay at (-0.25cm,0.3cm) {\includegraphics[width=13.7cm]{img/bifurk/screen-cb.png}};}

  \begin{tikzpicture}[remember picture,overlay]
    \only<1>{
      \draw [red!80,ultra thick] \boundellipse{0.7,0.4}{1}{0.3};
      \node[inner sep=1.5pt,label={60:Callback 1}] at (1.5,0.1) {};
      \draw[fill=cyan] (11.9,-2.6) circle (0cm) node[text=red] {1};
    }
    \only<3>{
      \draw [red!80,ultra thick] \boundellipse{5,-4.4}{2.3}{1.5};
      \node[inner sep=1.5pt,label={60:Callback 2}] at (4,-3) {};
      \draw[fill=cyan] (11.4,-2.6) circle (0cm) node[text=red] {2};
    }
    \only<5>{
      \draw [red!80,ultra thick] \boundellipse{0.5,-1.25}{1}{0.3};
      \node[inner sep=1.5pt,label={60:Callback 3}] at (1.6,-1.5) {};
      \draw[fill=cyan] (8.85,-1.35) circle (0cm) node[text=red] {3};
    }
  \end{tikzpicture}

  \only<2>{\lstBifurkInitValue}
  \only<4>{\lstBifurkFocus}
  \only<6>{\lstBifurkZoom}
  \insertlink{}
\end{frame}

\begin{frame}{Case Study: Exercises}
  \tikzoverlay at (-0cm,0.76cm) {\includegraphics[width=13.7cm]{img/bifurk/screen.png}};
  \begin{tikzpicture}[remember picture,overlay]
    \draw [red!80,ultra thick] \boundellipse{11.15,-4.4}{2.3}{1.5};
    \node[inner sep=1.5pt,label={60:Exercise 1},red] at (10,-3) {};

    \draw [red!80,ultra thick] \boundellipse{0.9,-4.8}{1}{0.5};
    \node[inner sep=1.5pt,label={60:Exercise 2},red] at (1.8,-5.1) {};

    \draw [red!80,ultra thick] \boundellipse{0.4,-5.65}{0.6}{0.3};
    \node[inner sep=1.5pt,label={60:Homework with solution},red] at (0.9,-6) {};

    \node[inner sep=1.5pt,label={60:Homework without solution},red] at (1.5,-2.4) {};
    \node[inner sep=1.5pt,label={60:More},red] at (0,-2.2) {};
    \node[inner sep=1.5pt,label={60:controls},red] at (-0.2,-2.5) {};
    \draw [red!80,ultra thick] \boundellipse{0.6,-2.1}{1}{0.5};
  \end{tikzpicture}
  \insertlink{}
\end{frame}

\begin{frame}{Case Study: Exercises}
  \vspace{-1em}
  \begin{block}{Exercise 1: `Number of solution' diagram}
    \begin{itemize}
    \item add \texttt{dcc.Graph} in the layout
    \item add \texttt{Output(id, 'figure')} in the \texttt{draw\_overview} callback
    \item build \texttt{go.Figure(data=[go.Scatter(x=count\_x, y=count\_y)])}
    \item add \texttt{go.layout.Annotation} text annotation
    \end{itemize}
  \end{block}
  \begin{block}{Exercice 2: `solutions for coef' text}
    \begin{itemize}
    \item add \texttt{html.Div} in the layout
    \item new callback with \texttt{Input('graph-overview', 'figure')}
    \item add state info \texttt{State('input-focus-coef', 'value')}
    \item build text with \texttt{solutions = graph['data'][1]['y']}
      \begin{itemize}
      \item (the solutions are already computed and plotted in red in the 2nd graph figure)
      \end{itemize}
    \end{itemize}
  \end{block}
  \insertlink{}
\end{frame}

\defverbatim[colored]\lstBifurkPermalink{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(Output('permalink', 'href'),
[Input(f"input-{input}", 'value') for input in INPUT_NAMES])
def get_permalink(*args):
    return "?"+"&".join(f"{k}={v}" \
           for k, v in zip(INPUT_NAMES, map(str, args)))
\end{lstlisting}
}

\defverbatim[colored]\lstBifurkReload{
\begin{lstlisting}[basicstyle=\ttfamily,language=python]
@app.callback(Output('page-content', 'children'),
              [Input('url', 'search')])
def display_page(search):
    search_dict = urllib.parse.parse_qs(search[1:])
    return build_layout(search_dict)
\end{lstlisting}
}

\begin{frame}{Case Study: Permalink homework}\vspace{-1.5em}
  \textbf{Key feature}, but not so easy to build ...

  \begin{exampleblock}{}
    \lstBifurkPermalink
  \end{exampleblock}
  \begin{exampleblock}{}
  \lstBifurkReload
  \end{exampleblock}
  \insertlink{}
\end{frame}
\end{document}
